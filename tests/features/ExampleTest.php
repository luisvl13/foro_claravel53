<?php

class ExampleTest extends FeatureTestCase
{

    function test_basic_example()
    {
        $user = factory(\App\User::class)->create([
            'name' => 'Luis Alberto',
            'email' => 'Luisvl13@Hotmail.com',
        ]);


        $this->actingAs($user,'api')
            ->visit('api/user')
             ->see('Luis Alberto')
             ->see('Luisvl13@Hotmail.com');
    }
}
